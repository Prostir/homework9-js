// Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// //     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// //     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

const tabsTitle = Array.from(document.getElementsByClassName('tabs-title'));

function centeredContent() {
    const title = Array.from(document.getElementsByClassName('tabs-title'));
    const dataTitle = this.getAttribute('data-title');
    const contentTab = Array.from(document.getElementsByClassName('text-content'));

    if (this.classList.contains('active')) {
        return;
    }
    title.forEach((value) => value.classList.remove('active'));
    this.classList.add('active');

    contentTab.forEach((value) => value.style.display = 'none');

    const dataTitleContent = contentTab.filter((items) => {
        return items.getAttribute('data-title') === dataTitle;
    });
    dataTitleContent[0].style.display = '';
}


tabsTitle.forEach((value) => value.addEventListener('click', centeredContent));
